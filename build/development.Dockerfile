FROM node:10.16.0-alpine

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install --only=production

COPY [".", "/usr/src/"]

RUN npm install --only=development

RUN npm run build

EXPOSE 3000

CMD ["node", "dist/index.js"]
