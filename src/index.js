const pow = (num, exp = 2) => num ** exp

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

const mapped = array.map(pow)

console.log(mapped)
